//
//  GameScene.h
//  Road Fighter
//

//  Copyright (c) 2016 Rameshkumarraju Sangaraju. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "JCButton.h"

@interface GameScene : SKScene <SKPhysicsContactDelegate>

@property (nonatomic, strong) SKLabelNode *velocityLabel;
@property (nonatomic, strong) SKLabelNode *scoreLabel;
@property (strong, nonatomic) JCButton* slowButton;

@end

static const int BUTTON_SIZE = 50;

static NSString *roadStr = @"Road";
static NSString *finishStr = @"Finish";
static NSString *backgroundStr = @"Sand";
static NSString *playerStr = @"player";
static NSString *enemyStr = @"Enemy";
static NSString *cameraTrackerStr = @"cameraTracker";
static NSString *cameraStr  = @"camera";
static NSString *meterStr = @"meter";


static const float PLAYER_SPEEDUP = 0.05f;
static const float PLAYER_SLOWDOWN = 0.12f;
static const float PLAYER_DIR = 0.0f;

static const float PLAYER_SPEEDX = 400.0f;

static const float MAX_SPEED = 640.0f;
static const float MIN_SPEED = 100.0f;

static const float ENEMY_MAX_SPEED = 600.0f;
static const float ENEMY_MIN_SPEED = 200.0f;

static const float SLOW_DELAY = 0.05f;

static const int NUM_ROADS = 100;
static const float X_RSCALE = 3.6;
static const float Y_RSCALE = 3.6;

static const float ENEMY_XSCALE = 1.9;
static const float ENEMY_YSCALE = 1.6;

static int score=0;

static int lives = 3;

