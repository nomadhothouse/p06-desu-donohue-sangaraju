//
//  AppDelegate.h
//  Road Fighter
//
//  Created by Rameshkumarraju Sangaraju on 3/8/16.
//  Copyright © 2016 Rameshkumarraju Sangaraju. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

