//
//  CategoryBitMasks.h
//  Road Fighter
//
//  Created by Thomas Donohue on 3/7/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#ifndef CategoryBitMasks_h
#define CategoryBitMasks_h

static const uint32_t playerCategory = 0x1 << 0;
static const uint32_t enemyCategory = 0x1 << 1;

static const uint32_t passThroughCategory = 0x1 << 4;



#endif /* CategoryBitMasks_h */