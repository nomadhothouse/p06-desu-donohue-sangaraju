//
//  GameScene.m
//  Road Fighter
//
//  Created by Rameshkumarraju Sangaraju on 3/8/16.
//  Copyright (c) 2016  Sangaraju. All rights reserved.
//

#define ARC4RANDOM_MAX 0x100000000
#define RANDI(min, max) (min + arc4random_uniform(max - min + 1))
#define RANDF(min, max) (((float)arc4random() / ARC4RANDOM_MAX) * (max - min) + min);
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@import CoreMotion;
@import AVFoundation;

#import "GameScene.h"
#import "JCButton.h"
#import "CGVectorAdditions.h"
#import "CategoryBitMasks.h"


@implementation GameScene
{
    SKNode *_roadNode;
    SKNode *_backgroundNode;
    SKNode *_playerNode;
    
    SKNode *_cameraTrackerNode;
    SKCameraNode *_cameraNode;
    
    float roadHeight;
    float nodeHeight;
    
    bool touchingLeft;
    bool touchingRight;
    bool isMaxSpeed;
    bool isMinSpeed;
    
    CGRect roadNodeDimensions;
    CGPoint currentPosition;
    
    SKAction *blink;
    SKAction *blinkForTime;
    
    //SpeedoCreation
    SKSpriteNode *_meter;
    SKSpriteNode *_needle;
    SKAction *rotation;
    
    float currAngle;
    float prevAngle;
    
    int currentRoad;
    
    NSMutableArray *playerLives;
    
    //Motion Manager
    CMMotionManager *_motionManager;
    
    //AV Player
    AVAudioPlayer *_playerSound;
    
    //SKStartLabel
    SKLabelNode *_playLabel;
    SKSpriteNode *_brakeNode;
    
    SKSpriteNode *_pauseNode;
}


//================================================INITIALIZE=============================================================

- (void)didMoveToView:(SKView *)view
{
    
    self.physicsWorld.contactDelegate = self;
    self.physicsWorld.gravity = CGVectorMake(0.0f, 0.0f);
    
    _backgroundNode = [self createBackgroundNode];
    [self addChild: _backgroundNode];
    
    _roadNode = [self createRoadNode];
    [self addChild: _roadNode];
    currentRoad = 0;
    
    _cameraTrackerNode = (SKNode *) [self childNodeWithName: cameraTrackerStr];
    _cameraTrackerNode.physicsBody.categoryBitMask = 0;
    _cameraTrackerNode.physicsBody.contactTestBitMask = 0;
    _cameraTrackerNode.physicsBody.collisionBitMask = passThroughCategory;
    isMaxSpeed = NO;
    
    _playerNode = (SKNode *) [self childNodeWithName: playerStr];
    _playerNode.physicsBody.categoryBitMask = playerCategory;
    _playerNode.physicsBody.contactTestBitMask = enemyCategory;
    _playerNode.physicsBody.collisionBitMask = passThroughCategory;
    
    _cameraNode = (SKCameraNode *) [_playerNode childNodeWithName: cameraStr];
    
    self.velocityLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    self.velocityLabel.fontSize = 30;
    self.velocityLabel.position = CGPointMake(-11 * self.size.width / 32, -0.1*self.size.height/4);
    self.velocityLabel.fontName = @"DBLCDTempBlack";
    self.velocityLabel.fontColor = [SKColor whiteColor];
    self.velocityLabel.zPosition = 25;
    [_cameraTrackerNode addChild: self.velocityLabel];
    
    self.scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    self.scoreLabel.fontSize = 35;
    self.scoreLabel.position = CGPointMake(self.size.width / 3, 36 * self.size.height / 32 + 40);
    self.scoreLabel.fontName = @"chalkDuster";
    self.scoreLabel.fontSize = 35.0f;
    [_cameraTrackerNode addChild:self.scoreLabel];
    UIColor *pcolor = Rgb2UIColor(128, 128, 128);
    self.slowButton = [[JCButton alloc] initWithButtonRadius: BUTTON_SIZE
                                                       color: [SKColor clearColor]
                                                pressedColor: [SKColor clearColor]
                                                     isTurbo: NO
                                                 isRapidFire: YES];

    self.slowButton.zPosition = 10;
    [self.slowButton setPosition:CGPointMake(self.size.width / 3-20, 30)];
    [_cameraTrackerNode addChild: self.slowButton];
    
    SKAction *slowDelay = [SKAction waitForDuration: SLOW_DELAY];
    SKAction *checkSlowButton = [SKAction runBlock:^{
        [self slowHandler];
    }];
    SKAction *checkSlowAction = [SKAction sequence:@[slowDelay, checkSlowButton]];
    [self runAction:[SKAction repeatActionForever: checkSlowAction]];
    
    
    blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],[SKAction fadeInWithDuration:0.1]]];
    blinkForTime = [SKAction repeatAction:blink count:5];
    
    [self createSpeedoMeter];
    
    rotation = [SKAction rotateByAngle: 0 duration:0];
    prevAngle = 0;
    [_needle runAction: rotation];
    
    [self createEnemies];
    [self createLives];
    
    //Initialize Motion
    _motionManager = [[CMMotionManager alloc] init];
    [self startTheGame];

    //Start Label//
    _playLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    _playLabel.text = @"Touch to Play!";
    _playLabel.fontSize = 45;
    _playLabel.zPosition = 30;
    _playLabel.position = CGPointMake(-self.size.width/6+100, self.size.height/2);
    [self addChild:_playLabel];
    
    //Add Brake
    _brakeNode = [SKSpriteNode spriteNodeWithImageNamed:@"brake1"];
    _brakeNode.position = CGPointMake(10 * self.size.width / 32,  0.25*self.size.height/8);
    [_cameraTrackerNode addChild:_brakeNode];
    
    /**_pauseNode = [SKSpriteNode spriteNodeWithImageNamed:@"Pause"];
    _pauseNode.name = @"Pause";
    _pauseNode.xScale = 0.1;
    _pauseNode.yScale = 0.1;
    _pauseNode.position = CGPointMake(-11 * self.size.width / 32, 36 * self.size.height / 32 + 40);

    [_cameraTrackerNode addChild:_pauseNode];**/

}

/*   ======================================== Road node creation =============================================  */

- (SKNode *) createRoadNode
{
    SKNode *roadNode = [SKNode node];
    roadHeight = 0.0f;
    
    SKSpriteNode *refNode = [SKSpriteNode spriteNodeWithImageNamed: roadStr];
    refNode .xScale = X_RSCALE;
    refNode .yScale = Y_RSCALE;
    nodeHeight = refNode.frame.size.height;
    
    for (int nodeCount = 0; nodeCount < NUM_ROADS; nodeCount++) {
        SKSpriteNode *node = [SKSpriteNode spriteNodeWithImageNamed: roadStr];
        node.zPosition = -10;
        node.xScale = X_RSCALE;
        node.yScale = Y_RSCALE;
        roadHeight += nodeHeight;
        node.position = CGPointMake(0.0f, nodeCount * nodeHeight  + (nodeHeight / 4));
        [roadNode addChild: node];
    }
    SKSpriteNode *finishLineNode = [SKSpriteNode spriteNodeWithImageNamed: finishStr];
    finishLineNode.zPosition = -1;
    finishLineNode.xScale = 2.1;
    finishLineNode.yScale = 2.2;
    finishLineNode.position = CGPointMake(0.0f, (NUM_ROADS - 1) *  nodeHeight);
    [roadNode addChild: finishLineNode];
    SKSpriteNode *finalNode = [SKSpriteNode spriteNodeWithImageNamed: roadStr];
    
    finalNode.zPosition = -10;
    finalNode.xScale = X_RSCALE;
    finalNode.yScale = Y_RSCALE;
    finalNode.position = CGPointMake(0.0f, NUM_ROADS *  nodeHeight + (nodeHeight / 4));
    [roadNode addChild: finalNode];
    
    
    return roadNode;
}

/*   ==================================  Background node creation  =================================  */

- (SKNode *) createBackgroundNode
{
    SKNode *backgroundNode = [SKNode node];
    
    for (int nodeCount = 0; nodeCount < NUM_ROADS * 2; nodeCount++) {
        SKSpriteNode *node = [SKSpriteNode spriteNodeWithImageNamed: backgroundStr];
        node.zPosition = -100;
        //node.xScale = X_RSCALE;
        //node.yScale = Y_RSCALE;
        node.position = CGPointMake(0.0f, nodeCount * node.frame.size.height);
        [backgroundNode addChild: node];
    }
    
    return backgroundNode;
}



/* =================================  Speedometer creation  ================================= */

- (void) createSpeedoMeter
{
    _meter = [SKSpriteNode spriteNodeWithImageNamed:@"meter1"];
    _meter.xScale = 0.3;
    _meter.yScale = 0.3;
    _meter.zPosition = 0;
    [_meter setPosition:CGPointMake(-11 * self.size.width / 32,  0.25*self.size.height/8)];
    [_cameraTrackerNode addChild:_meter];
    
    _needle = [SKSpriteNode spriteNodeWithImageNamed:@"needle"];
    _needle.xScale = 0.3;
    _needle.yScale = 0.3;
    _needle.zPosition = 20;
    [_needle setPosition:CGPointMake(-11 * self.size.width / 32, 0.25*self.size.height/8)];
    [_cameraTrackerNode addChild:_needle];
}

/* ============================== Enemy cars creation  ==========================================*/
- (void) createEnemies
{
    SKNode *nextRoad = [_roadNode.children objectAtIndex: currentRoad + 1];
    
    SKSpriteNode *enemyNode = [SKSpriteNode spriteNodeWithImageNamed: enemyStr];
    enemyNode.xScale = ENEMY_XSCALE;
    enemyNode.yScale = ENEMY_YSCALE;
    roadNodeDimensions = [_roadNode calculateAccumulatedFrame];
    float randXPos = nextRoad.position.x  + RANDF(((-roadNodeDimensions.size.width / 2) + enemyNode.frame.size.width/4), ((roadNodeDimensions.size.width / 2) - enemyNode.frame.size.width/4));
    float randYPos =  nextRoad.position.y + RANDF(-self.size.height / 2, self.size.height / 2);
    float randSpeed = RANDF(ENEMY_MIN_SPEED, ENEMY_MAX_SPEED);
    enemyNode.position = CGPointMake(randXPos,  randYPos);
    enemyNode.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: enemyNode.size];
    enemyNode.physicsBody.mass = _playerNode.physicsBody.mass;
    enemyNode.physicsBody.friction = 0.0f;
    enemyNode.physicsBody.restitution = 0.0f;
    enemyNode.physicsBody.dynamic = YES;
    enemyNode.physicsBody.affectedByGravity = NO;
    enemyNode.physicsBody.categoryBitMask = enemyCategory;
    enemyNode.physicsBody.contactTestBitMask = playerCategory;
    enemyNode.physicsBody.collisionBitMask = passThroughCategory;
    enemyNode.physicsBody.velocity = CGVectorMake(0, randSpeed);
    [self addChild: enemyNode];
    
}

/*  =========================== creating cars that shows remaining lives ========================== */
- (void) createLives {
    
    playerLives = [[NSMutableArray alloc] init];
    CGPoint location;
    for(int i=0;i<lives;i++) {
        SKSpriteNode *playerlive = [SKSpriteNode spriteNodeWithImageNamed:@"car"];
        location.y = 36 * self.size.height / 32 - (_playerNode.frame.size.height/8);
        location.x = self.size.width/3 - 50 + (i*0.8*_playerNode.frame.size.width);
        playerlive.position = location;
        playerlive.xScale = 0.3;
        playerlive.yScale = 0.3;
        [playerLives addObject:playerlive];
        [_cameraTrackerNode addChild:playerlive];
    }
}

/* ==================================== starting the game here! ================================== */
- (void)startTheGame
{
    _playerNode.hidden = NO;
    [self gameStartSoundEffect];
    [self startPlayerSound];
    //reset ship position for new game
    [_playerNode setPosition:(CGPointMake(6, _playerNode.position.y))];
    //setup to handle accelerometer readings using CoreMotion Framework
    [self startMonitoringAcceleration];
    
}

/*  ===================================  Monitoring accelaration ================================ */
- (void)startMonitoringAcceleration
{
    if (_motionManager.accelerometerAvailable) {
        [_motionManager startAccelerometerUpdates];
        NSLog(@"accelerometer updates on...");
    }
}

- (void)stopMonitoringAcceleration
{
    if (_motionManager.accelerometerAvailable && _motionManager.accelerometerActive) {
        [_motionManager stopAccelerometerUpdates];
        NSLog(@"accelerometer updates off...");
    }
}

- (void)updateShipPositionFromMotionManager
{
    CMAccelerometerData* data = _motionManager.accelerometerData;
    if (fabs(data.acceleration.x) > 0.2) {
        //[_playerNode.physicsBody applyForce:CGVectorMake(0.0, 40.0 * data.acceleration.x)];
    }
}


//=============================================================UPDATE=========================================================

- (void) update:(CFTimeInterval) currentTime
{
    if(!_playLabel.hidden)
        return;
    if(_playerNode.position.y < roadHeight - nodeHeight){
        
        CGVector playerDir = CGConvertAngleToVector(PLAYER_DIR);
        
        if(_playerNode.physicsBody.velocity.dy < MAX_SPEED && !isMaxSpeed){
            if(!isMinSpeed) {
                [_playerNode.physicsBody applyImpulse: CGVectorMultiplyByScalar(playerDir, PLAYER_SPEEDUP)];
                _cameraTrackerNode.physicsBody.velocity = CGVectorMake(0, _playerNode.physicsBody.velocity.dy);
            }
        }else{
            
            isMaxSpeed = YES;
            _cameraTrackerNode.physicsBody.velocity = CGVectorMake(0, MAX_SPEED);
            _playerNode.physicsBody.velocity = CGVectorMake(0, MAX_SPEED);
        }
        [self updateVelocity];
        
    } else {
        _cameraTrackerNode.physicsBody.velocity = CGVectorMake(0, 0);
        _playerNode.physicsBody.velocity = CGVectorMake(0, 0);
    }
    
    //checking if player node touching the road boundaries
    roadNodeDimensions = [_roadNode calculateAccumulatedFrame];
    if((-roadNodeDimensions.size.width/2 + _playerNode.frame.size.width / 3 > _playerNode.position.x) || (_playerNode.position.x > roadNodeDimensions.size.width/2 - _playerNode.frame.size.width/3)) {
        [_playerNode runAction:blinkForTime];
        [self lostLife];
        [self resetToMinSpeed];
    }
    
    float speed = _playerNode.physicsBody.velocity.dy;
    
    [self playerSoundAction: speed];
    
    if(touchingLeft) {
        // for allowing player movement
        _playerNode.physicsBody.velocity = CGVectorMake(-PLAYER_SPEEDX,  _playerNode.physicsBody.velocity.dy);
        
    }else if(touchingRight) {

        _playerNode.physicsBody.velocity = CGVectorMake(PLAYER_SPEEDX,  _playerNode.physicsBody.velocity.dy);
        
    } else {
        _playerNode.physicsBody.velocity = CGVectorMake(0,  _playerNode.physicsBody.velocity.dy);
    }
    
    if(currentRoad != [self getCurrentRoadIndex]) {
        currentRoad = [self getCurrentRoadIndex];
        //NSLog(@"road %d", currentRoad);
        [self createEnemies];
    }
    [self updateScore];
    
    //UpdateMotion
    [self updateShipPositionFromMotionManager];
}

- (void) didBeginContact: (SKPhysicsContact*) contact
{
    if(contact.bodyA.categoryBitMask == playerCategory &&
       contact.bodyB.categoryBitMask == enemyCategory) {
        SKNode *enemy = contact.bodyB.node;
        CGVector playerDir = CGConvertAngleToVector(0.0f);
        [enemy.physicsBody applyImpulse: CGVectorMultiplyByScalar(playerDir, 0.5f)];
        
        [_playerNode runAction:blinkForTime];
        [self lostLife];
        [self resetToMinSpeed];
    }
}

/*  ================================= taking care of lives ============================= */
- (void) lostLife {
    
    if(--lives < 0) {
        //[self playerCrashedSoundEffect];
        //[self gameOverSoundEffect];
        [_playerSound pause];
        _playerNode.hidden = YES;
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Game Over"
                                                       message: @"No Remaining Lives"
                                                      delegate: self
                                             cancelButtonTitle:@ "Replay"
                                             otherButtonTitles:@"Exit",nil];
        self.scene.paused = YES;
        [alert show];
    } else {
        [self playerCrashedSoundEffect];
        [_playerNode setPosition:(CGPointMake(6, _playerNode.position.y))];
        //_playerNode.position = CGPointMake(6, _playerNode.position.y);
        SKSpriteNode *removeLife = [playerLives objectAtIndex: playerLives.count - 1];
        [removeLife removeFromParent];
        [playerLives removeObjectAtIndex: playerLives.count - 1];
    }
}

/* ==================================== Sound effects to game =================================== */

- (void) gameStartSoundEffect{
    SKAction *gameStartSoundAction = [SKAction playSoundFileNamed:@"Road_fighter_race_start.mp3" waitForCompletion:YES];
    [_playerNode runAction:gameStartSoundAction];
}

- (void) playerCrashedSoundEffect{
    SKAction *playerCrashedSoundAction = [SKAction playSoundFileNamed:@"car_crash-2.mp3" waitForCompletion:YES];
    [_playerNode runAction:playerCrashedSoundAction];
}

- (void) gameOverSoundEffect{
    SKAction *gameStartSoundAction = [SKAction playSoundFileNamed:@"Road_Fighter_game_over1.mp3" waitForCompletion:YES];
    [_playerNode runAction:gameStartSoundAction];
}

- (void) playerSoundAction: (float)speed{
    SKAction *playerStartSoundAction = [SKAction playSoundFileNamed:@"car_sound_starting.mp3" waitForCompletion:YES];
    
    NSTimeInterval now = _playerSound.deviceCurrentTime;
    if(speed <= MIN_SPEED){
        //NSLog(@"start music");
        [_playerSound pause];
        [_playerNode runAction:playerStartSoundAction];
    }else if(![_playerSound isPlaying]){
        
        [_playerSound       playAtTime: now + 2];
    }
}

- (void) startPlayerSound{
    NSError *err;
    NSURL *file = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"car_sound_max_speed.mp3" ofType:nil]];
    _playerSound = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:&err];
    if (err) {
        NSLog(@"error in audio play %@",[err userInfo]);
        return;
    }
    [_playerSound prepareToPlay];
    _playerSound.numberOfLoops = -1;
}

//==========================================================HANDLERS==========================================================


- (void) slowHandler
{
    if(self.slowButton.wasPressed) {
        isMaxSpeed = NO;
        if(_playerNode.physicsBody.velocity.dy > MIN_SPEED && !isMinSpeed){
            CGVector playerDir = CGConvertAngleToVector(PLAYER_DIR);
            [_playerNode.physicsBody applyImpulse: CGVectorMultiplyByScalar(playerDir, -PLAYER_SLOWDOWN)];
            _cameraTrackerNode.physicsBody.velocity = CGVectorMake(0, _playerNode.physicsBody.velocity.dy);
        } else {
            [self resetToMinSpeed];
        }
    } else {
        isMinSpeed = NO;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    
    /**for(UITouch *touch in touches) {
        CGPoint touchLocation = [touch locationInNode: self];
        SKNode *node = [self nodeAtPoint: touchLocation];
        if([node.name  isEqual: @"Pause"]) {
            [_playerSound pause];
            _playerNode.hidden = YES;
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Game Paused"
                                                           message: nil
                                                          delegate: self
                                                 cancelButtonTitle: @"Resume"
                                                 otherButtonTitles: nil,
                                                    nil];
            self.scene.paused = YES;
            [alert show];
        }
        
    }**/
    
    
    if(!_playLabel.hidden)
        _playLabel.hidden = YES;
    if (touchLocation.x < _playerNode.frame.origin.x) {
        touchingLeft = YES;
        touchingRight = NO;
    }
    
    if (touchLocation.x > _playerNode.frame.origin.x) {
        touchingRight = YES;
        touchingLeft = NO;
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(touchingLeft){
        touchingLeft = NO;
    }
    if(touchingRight){
        touchingRight = NO;
    }
}

//==========================================================UTILITIES==========================================================
- (void)reset {
    _playerNode.hidden = NO;
    [_velocityLabel removeFromParent];
    [_scoreLabel removeFromParent];
    [_needle removeFromParent];
    [_slowButton removeFromParent];
    [_meter removeFromParent];
    [_roadNode removeFromParent];
    
    score = 0;
    lives = 3;
    touchingLeft = NO;
    touchingRight = NO;
    /*[_playerNode removeFromParent];*/
    //[_cameraNode removeFromParent];
    //[_cameraTrackerNode removeFromParent];
    
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex]) {
        exit(0);
    }
    else {
        [self reset];
        self.scene.paused = NO;
        [self didMoveToView:self.view];
    }
}

- (void) updateScore
{
    score += 1;
    self.scoreLabel.text = [NSString stringWithFormat:@"%d",score];
    
}
- (void) updateVelocity
{
    float maxAngle = -(3*M_PI)/2;
    currAngle = ((_cameraTrackerNode.physicsBody.velocity.dy * maxAngle)/(MAX_SPEED));
    float angle = currAngle - prevAngle;
    prevAngle = currAngle;
    /**NSLog(@"velocity is:%f", _cameraTrackerNode.physicsBody.velocity.dy);
     NSLog(@"Angle is:%f", angle);
     NSLog(@"Current Angle is:%f", currAngle);
     NSLog(@"prev angle is:%f", prevAngle);**/
    rotation = [SKAction rotateByAngle: angle duration:0];
    [_needle runAction:rotation];
    self.velocityLabel.text =[NSString stringWithFormat:@"%.01f", _cameraTrackerNode.physicsBody.velocity.dy ];
}

- (void) resetToMinSpeed
{
    //NSLog(@"reset to min speed called %d", lives);
    isMinSpeed = YES;
    isMaxSpeed = NO;
    _cameraTrackerNode.physicsBody.velocity = CGVectorMake(0, MIN_SPEED);
    _playerNode.physicsBody.velocity = CGVectorMake(0, MIN_SPEED);
    
}

- (int) getCurrentRoadIndex
{
    for(int i = 0; i < [_roadNode.children count]; i++){
        if(CGRectIntersectsRect(_playerNode.frame, [_roadNode.children objectAtIndex: i].frame)){
            return i;
        }
    }
    return -1;
}

@end
