//
//  Player.h
//  Road Fighter
//
//  Created by Thomas Donohue on 3/7/16.
//  Copyright © 2016 Thomas Donohue. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Player : SKNode

-(id) initWithNode: (SKNode*) initNode;

@end
